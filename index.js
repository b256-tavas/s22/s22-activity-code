let registeredUsers = [
  "James Jeffries",
  "Gunther Smith",
  "Macie West",
  "Michelle Queen",
  "Shane Miguelito",
  "Fernando Dela Cruz",
  "Akiko Yukihime"
];

function registerUser(username) {
  if (registeredUsers.includes(username)) {
    alert("Registration failed. Username already exists!");
  } else {
    registeredUsers.push(username);
    alert("Thank you for registering!");
  }
}

registerUser("John Doe"); 

console.log(registeredUsers);

let friendList = [];

function addFriend(username) {
  if (registeredUsers.includes(username)) {
    friendList.push(username);
    alert(`You have added ${username} as a friend!`);
  } else {
    alert("User not found.");
  }
}

addFriend("Macie West");

console.log(friendList);

function displayFriends() {
  if (friendList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    friendList.forEach(friend => console.log(friend));
  }
}

displayFriends(); 

function countFriends() {
  if (friendList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    alert(`You currently have ${friendList.length} friends.`);
  }
}

countFriends();

function deleteLastFriend() {
  if (friendList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    friendList.pop();
  }
}

deleteLastFriend();

console.log(friendList);
